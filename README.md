Nicole St. Clair

Dr. Casmier

English 1900

9/15/17

# Unified

The world around us is full of choices. Each day, we choose to get out of bed, which clothes we wear, what we eat, which way we walk to class. The choices are countless, and most of the time we do not even realize that we are making a choice. But are we actually able to choose? The increasingly connected networks of the world have altered our behaviors and removed our choices from us slowly and without our knowing. We dress a specific way because of the media. We eat specific foods because of how we want to look. We walk to class because technology has told us it’s the shortest way. 

Shaviro’s work, “Connected”, reveals the intimate relationship that humankind has with the technium described by Kelly. As independent, living beings, we like to think of ourselves as in control of our own personal lives, but as Shaviro said, “The network is impersonal, universal, without a center, but it was also perturbingly intimate, uncannily close at hand.” The technium is all around us, even within us, yet we think of it as though it is something for us to choose to access when we want to. In reality, the network is always there, always working in the background. Our perception of it is not the reality that we do not see. 

![alt text](https://www.cybersecurity-insiders.com/wp-content/uploads/2017/03/network-web-generic-02.png)
Even writing this paper, I was given the illusion of choice. I started it in Microsoft Word, a platform that removes control from me, in order to make it easier and faster to write. With that ease of use, however, I lost control over specific commands and formatting within this essay. This is a trend that you see more and more, especially if you look closely, within daily life. Autocorrect on our phones and computers anticipates what you are going to say or attempting to say, through our previous words and the letters we use in misspelled words. Word processors, such as Microsoft Word have programs such as autocorrect embedded within them, allowing for those red squiggly lines to appear, letting me know that I screwed up. 

Markdown, on the other hand, allows the user to control more of the small steps within the process of writing a paper. To give your document a title is more than just highlighting and pressing a button. You must use the specific code to achieve the look that you want it to have. Every alteration of the text has a specific written command, allowing the user to tell the processor exactly what they want it to do, rather than the processor anticipating the user. A mistype may result in a format that the user did not intend to have, but it resulted because the directions were there. There was a learning curve when I first started using Markdown. Imbedding a picture was fairly easy; however, basic formatting was difficult due to the controls that I was forced to use to get my paper to look a certain way.

![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1200px-Markdown-mark.svg.png)
This is not to say that Markdown gives the user complete control of their writing process. The system has to translate what the writer says into the formatting, therefore there are specific commands that must be used. If you want something to be bold, you must give it a specific command, rather than just typing the word “bold” next to what you want to edit. Variation in these commands would result in an undesirable effect on the formatting and the appearance of the project. The user does not have complete control, similar to every other facet of life. We have to conform to what is expected of us, be it within society or within a word processor. 

The connectedness of the world around us has made us look for the easy way to do everything. By choosing the _easy_ way out, we lose out on the control that we think is there. The network truly runs everything, from our internet to miniscule choices we each make everyday. Our choices are often far from conscious, yet they still add up, and, as Shaviro says, “Every connection has it’s price; the one thing you can be sure of is that, sooner or later, you will have to pay.”
